import { ActionTypes } from "../constants/action-types";
const initialState = {
  events: [],
};
export const eventReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SET_EVENTS:
      return { ...state, events: action.payload };

    default:
      return state;
  }
};
