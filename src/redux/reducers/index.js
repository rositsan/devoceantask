import { combineReducers } from "redux";
import { eventReducer } from "./event-reducer";

const reducers = combineReducers({
  allEvents: eventReducer,
});

export default reducers;
