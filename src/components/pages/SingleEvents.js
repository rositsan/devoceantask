import React from "react";
import { useSelector } from "react-redux";
import { Table } from "react-bootstrap";
import "./../../styles/pages/SingleEvent.css";
import SimpleButton from "../UI/SimpleButton";

const SingleEvent = (props) => {
  const history = props.history;
  const events = useSelector((state) => state);
  const selectedEvent = events.allEvents.events.filter(
    (event) => event.id === props.match.params.id
  );

  const id = selectedEvent[0]?.id;
  const image = selectedEvent[0]?.image;
  const address = selectedEvent[0]?.address;
  const phone = selectedEvent[0]?.phone;
  const email = selectedEvent[0]?.email;
  let nearbyPlaces = [];
  nearbyPlaces = events.allEvents.events.filter((ev) => {
    return ev.address.city === address.city && ev.id !== id;
  });

  return (
    <div className="event-main-container">
      {selectedEvent[0] ? (
        <div>
          <div className="event-image">
            <img src={image} alt="selectedEvent" className="image" />
          </div>
          <div className="event-details">
            <div className="event-address">
              <h4>Address</h4>
              <span>
                {address.number} {address.street}
                <br></br>
                {address.city}, {address.zip}
              </span>
            </div>
            <div className="event-contact">
              <h4>Contact</h4>
              <span>
                {phone}
                <br></br>
                {email}
              </span>
            </div>
            <div className="event-nearby-places">
              <h4>Nearby Places</h4>
              <Table borderless>
                <tbody>
                  {nearbyPlaces
                    ? nearbyPlaces.map((place) => (
                        <tr key={place.id}>
                          <td>
                            <span>{place.name}</span>
                          </td>
                          <td>
                            <span>
                              {place.address.number} {place.address.street}{" "}
                              {place.address.city} {place.address.country}{" "}
                              {place.address.zip}
                            </span>
                          </td>
                        </tr>
                      ))
                    : "No nearby places"}
                </tbody>
              </Table>
            </div>
            <SimpleButton onClick={() => history.push("/")} />
          </div>
        </div>
      ) : (
        "Event does not exist"
      )}
    </div>
  );
};

export default SingleEvent;
