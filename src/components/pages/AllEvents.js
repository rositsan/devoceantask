import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { setEvents } from "../../redux/actions/event-actions";
import { Table } from "react-bootstrap";
import "./../../styles/pages/AllEvents.css";
import Loader from "../UI/Loader";

const url = "https://api.jsonbin.io/b/6177e9399548541c29c8c0f5";

const AllEvents = ({ history }) => {
  const events = useSelector((state) => state);
  const dispatch = useDispatch();

  const fetchData = async () => {
    const response = await axios.get(url).catch((error) => console.log(error));
    if (!response) return null;
    dispatch(setEvents(response.data));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      {!events.allEvents ? (
        <div className="loader">
          <Loader />
        </div>
      ) : (
        <div className="main-content">
          <div className="main-content-table">
            <Table borderless hover>
              <thead>
                <tr>
                  <th className="content-table">NAME</th>
                  <th className="content-table">DESCRIPTION</th>
                </tr>
              </thead>
              <tbody>
                {events.allEvents
                  ? events.allEvents.events.map((event) => (
                      <tr
                        key={event.id}
                        onClick={() => history.push(`events/${event.id}`)}
                      >
                        <td>{event.name}</td>
                        <td>{event.description}</td>
                      </tr>
                    ))
                  : "No business to show"}
              </tbody>
            </Table>
          </div>
        </div>
      )}
    </div>
  );
};

export default AllEvents;
