import { Spinner } from "react-bootstrap";
import "./../../styles/UI/Loader.css";

const Loader = () => {
  return (
    <Spinner
      className="spinner"
      animation="border"
      role="status"
      style={{ width: "100px", height: "100px" }}
    >
      <span className="visually-hidden">Loading...</span>
    </Spinner>
  );
};

export default Loader;
