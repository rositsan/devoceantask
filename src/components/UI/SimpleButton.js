import React from "react";
import "./../../styles/UI/SimpleButton.css";

const SimpleButton = (props) => {
  const onClick = props.onClick;

  return (
    <div className="button" onClick={onClick}>
      {props.children}
    </div>
  );
};

export default SimpleButton;
