import React from 'react';
import { Link } from 'react-router-dom';
import "./../../styles/header/Logo.css";

const Logo = () => {
    return <Link to="/" className="logo"></Link>;
}

export default Logo;
