import React from "react";
import Logo from "./Logo";
import "./../../styles/header/Header.css";

const Header = () => {
  return (
    <header>
      <Logo />
    </header>
  );
};

export default Header;
