import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/header/Header";
import AllEvents from "./components/pages/AllEvents";
import SingleEvent from "./components/pages/SingleEvents";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <Switch>
          <Route path="/" component={AllEvents} exact />
          <Route path="/events/:id" component={SingleEvent} />
          <Route>Error 404 page not found</Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
